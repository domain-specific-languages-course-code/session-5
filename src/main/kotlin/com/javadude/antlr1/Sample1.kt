package com.javadude.antlr1

import org.antlr.v4.runtime.CharStreams
import org.antlr.v4.runtime.CommonTokenStream
import org.antlr.v4.runtime.ParserRuleContext
import org.antlr.v4.runtime.tree.ErrorNode
import org.antlr.v4.runtime.tree.ParseTreeWalker
import org.antlr.v4.runtime.tree.TerminalNode
import java.util.*


class Sample1 {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            println("Hello, World!")

            val input = CharStreams.fromFileName("expr1.expressions")
            val lexer = Expressions3Lexer(input)
            val tokens = CommonTokenStream(lexer)
            val parser = Expressions3Parser(tokens)

            parser.buildParseTree = true
            val tree = parser.expressionList()
            println(tree!!.toStringTree(parser))

            ParseTreeWalker.DEFAULT.walk(MyExpressionsListener(), tree)
        }
    }

}

class Calc {
    var variables = mutableMapOf<String, Int>()
    fun op(op : String, op1 : Int, op2 : Int) =
        when (op) {
            "*" -> op1 * op2
            "/" -> op1 / op2
            "+" -> op1 + op2
            "-" -> op1 - op2
            else -> throw IllegalArgumentException()
        }

    fun getVariable(name : String) = variables[name]
    fun setVariable(name : String, value : Int) {
        variables[name] = value
    }
}

class MyExpressionsListener : Expressions3Listener {
    val calc = Calc()
    val stack = Stack<Int>()
    override fun exitExpressionList(ctx: Expressions3Parser.ExpressionListContext) {
        val result = stack.pop()
        println(result)
    }


    override fun exitAssignment(ctx: Expressions3Parser.AssignmentContext) {
        val value = stack.pop()
        calc.setVariable(ctx.name.text, value)
    }


    override fun exitExpression(ctx: Expressions3Parser.ExpressionContext) {
        if (ctx.variable != null) {
            stack.push(calc.getVariable(ctx.variable.text))

        } else if (ctx.intLiteral != null) {
            stack.push(ctx.intLiteral.text.toInt())

        } else {
            val rhs = stack.pop()
            val lhs = stack.pop()
            stack.push(calc.op(ctx.op.text, lhs, rhs))
        }
    }



    override fun enterExpression(ctx: Expressions3Parser.ExpressionContext) {}
    override fun enterAssignment(ctx: Expressions3Parser.AssignmentContext) {}
    override fun enterExpressionList(ctx: Expressions3Parser.ExpressionListContext) {}
    override fun enterEveryRule(ctx: ParserRuleContext) {}
    override fun exitEveryRule(ctx: ParserRuleContext) {}
    override fun visitErrorNode(node: ErrorNode) {}
    override fun visitTerminal(node: TerminalNode) {}
}
