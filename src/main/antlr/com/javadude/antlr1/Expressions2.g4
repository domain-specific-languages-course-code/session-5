grammar Expressions2;

@header {
    package com.javadude.antlr1;
    import java.util.Map;
    import java.util.HashMap;
}

@members {
    Calc calc = new Calc();
}

//lower-case start - parser rule
//upper-case start - lexer rule

expressionList returns [int value]
    :   (   assignment
        |   expression
        )*
        last=expression { $value = $last.value; }
        EOF
    ;

assignment
    :   name=ID '=' expression { calc.setVariable($name.text, $expression.value); }
    ;

expression returns [int value = 0]
    :  op1=expression op=( '*' | '/' ) op2=expression { $value = calc.op($op.text, $op1.value, $op2.value); }
    |  op1=expression op=( '+' | '-' ) op2=expression { $value = calc.op($op.text, $op1.value, $op2.value); }
    |  ID  { $value = calc.getVariable($ID.text); }
    |  INT  { $value = $INT.int; }
    ;

ID : ('a'..'z'|'A'..'Z') ('a'..'z'|'A'..'Z'|'0'..'9')* ;
INT : ('0'..'9')+ ;
WS : (' ' | '\n' | '\r' | '\t')+ -> skip ;
