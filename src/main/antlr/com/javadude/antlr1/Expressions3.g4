grammar Expressions3;

@header {
    package com.javadude.antlr1;
}

//lower-case start - parser rule
//upper-case start - lexer rule

expressionList
    :   (   assignment
        |   expression
        )*
        expression
        EOF
    ;

assignment
    :   name=ID '=' expression
    ;

expression
    :  expression op=( '*' | '/' ) expression
    |  expression op=( '+' | '-' ) expression
    |  variable=ID
    |  intLiteral=INT
    ;

ID : ('a'..'z'|'A'..'Z') ('a'..'z'|'A'..'Z'|'0'..'9')* ;
INT : ('0'..'9')+ ;
WS : (' ' | '\n' | '\r' | '\t')+ -> skip ;

STRING : '"' .*? '"' ;
