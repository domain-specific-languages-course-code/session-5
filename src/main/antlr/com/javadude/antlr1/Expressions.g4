grammar Expressions;

@header {
    package com.javadude.antlr1;
    import java.util.Map;
    import java.util.HashMap;
}

@members {
    Map<String, Integer> variables = new HashMap<>();
}

//lower-case start - parser rule
//upper-case start - lexer rule

expressionList returns [int value]
    :   (   assignment
        |   expression
        )*
        last=expression { $value = $last.value; }
        EOF
    ;

assignment
    :   name=ID '=' expression { variables.put($name.text, $expression.value); }
    ;

expression returns [int value = 0]
    :  op1=expression op=( '*' | '/' ) op2=expression
        {
            if ("*".equals($op.text)) {
                $value = $op1.value * $op2.value;
            } else {
                $value = $op1.value / $op2.value;
            }
        }
    |  op1=expression op=( '+' | '-') op2=expression
        {
            if ("+".equals($op.text)) {
                $value = $op1.value + $op2.value;
            } else {
                $value = $op1.value - $op2.value;
            }
        }
//    :  op1=expression
//        ( '*' op2=expression { $value = $op1.value * $op2.value; }
//        | '/' op2=expression { $value = $op1.value / $op2.value; }
//        )
//    |  op1=expression
//        ( '+' op2=expression { $value = $op1.value + $op2.value; }
//        | '-' op2=expression { $value = $op1.value - $op2.value; }
//        )
    |  ID  { $value = variables.get($ID.text); }
    |  INT  { $value = $INT.int; }
    ;

ID : ('a'..'z'|'A'..'Z') ('a'..'z'|'A'..'Z'|'0'..'9')* ;
INT : ('0'..'9')+ ;
WS : (' ' | '\n' | '\r' | '\t')+ -> skip ;
